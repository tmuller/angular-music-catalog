# Musikkatalog Demo Projekt

Dieses Projekt soll das Interface von iTunes imitieren und demonstriert einige
nicht alltägliche Features von Angular:

1. Custom Form Controls in Form eines Zeitdauerselektors für Minuten und Sekunden,
   ohne die Limitierung des HTML ``<input type="time" />`` Elements (commit 3068d176)
1. Dynamischer Austausch von Komponenten um einen Datensatz auf verschiedene Weise 
   anzuzeigen (commits 7239507f bis 8ce4c57c)
1. Auto-complete Implementierung (hier von Künstler- oder Bandnamen) 
   (e240682a und 9329aeb9)
1. Einfache Implementierung von mehreren Komponenten auf einer Seite durch mehrfachen,
   geschachtelten Gebrauch von ``<router-outlet />``
1. Implementiert komplexe, geschachtelte, reaktive Formulare (Album -> mehrere Disks -> 
   mehrere Songs pro Disk), Implementierung der Bearbeitung von geschachtelten Formularen 
   in HTML     
1. Implementierung der Uebergabe von Templateschnipseln an Childkomponenten um die
   Flexibilitaet und wiederverwendbarkeit von Elementen zu erhoehen.   
