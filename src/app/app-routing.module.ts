import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TlistComponent } from './test-list/tlist/tlist.component';
import { TestWrapperComponent } from './test-wrapper/test-wrapper.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/tlist', pathMatch: 'full' },
  { path: 'tlist', component: TestWrapperComponent},
  { path: 'catalog',
    loadChildren: () => import('./catalog/catalog.module').then(mod => mod.CatalogModule)},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
