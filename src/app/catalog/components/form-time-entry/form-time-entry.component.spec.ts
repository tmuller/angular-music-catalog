import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTimeEntryComponent } from './form-time-entry.component';

describe('FormTimeEntryComponent', () => {
  let component: FormTimeEntryComponent;
  let fixture: ComponentFixture<FormTimeEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTimeEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTimeEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
