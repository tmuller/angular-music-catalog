import { Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-form-time-entry',
  templateUrl: './form-time-entry.component.html',
  styleUrls: ['./form-time-entry.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => FormTimeEntryComponent),
    multi: true,
  }]
})
export class FormTimeEntryComponent implements ControlValueAccessor {

  @ViewChild('durationEntry', {read: ElementRef, static: true})
  durationElement: ElementRef;

  @Input() durationInternal: string; // string of format mm:ss

  @Input() private minuteValue = '';
  @Input() private secondValue = '';

  constructor() { }

  private propagateChange;

  public getLengthValue(): void {
    this.minuteValue = this.getFormFieldVal(0);
    this.secondValue = this.getFormFieldVal(1);
    const timeValue = `${this.minuteValue}:${this.secondValue}`;
    this.propagateChange(timeValue);
  }

  private getFormFieldVal(fieldNum: number): string {
    const fieldVal = this.durationElement.nativeElement.childNodes[0].children[fieldNum].value;
    return this.forceDoubleDigits(fieldVal);
  }

  private forceDoubleDigits(numberValue: string): string {
    return `00${ numberValue }`.substr(-2);
  }

  public registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: any): void {}

  public writeValue(timeinMinAndSec: string): void {
    const [minutes, seconds, ...rest] = timeinMinAndSec.split(':'); // Format mm:ss
    this.minuteValue = minutes;
    this.secondValue = seconds;
  }

}
