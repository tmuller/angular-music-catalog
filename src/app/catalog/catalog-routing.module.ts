import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AlbumEditComponent, AlbumFormComponent, AlbumListComponent } from '.';
import { ArtistListComponent, ArtistEditComponent, ArtistDetailComponent } from '.';
import { CatalogComponent } from './catalog.component';
import { ArtistResolverService } from './services/artist-resolver.service';

const catalogRoutes: Routes = [
  {
    path: 'catalog',
    component: CatalogComponent,
    children: [
      {
        path: 'artist',
        component: ArtistListComponent,
        resolve: {artists: ArtistResolverService},
        children: [
          {path: 'detail/:id', component: ArtistDetailComponent},
          {path: 'edit', component: ArtistEditComponent},
          {path: 'edit/:id', component: ArtistEditComponent},
        ]
      },
      {
        path: 'albums',
        component: ArtistListComponent,
        resolve: {artists: ArtistResolverService},
        children: [
          {path: 'detail', component: AlbumEditComponent},
          {path: 'detail/:albumId', component: AlbumEditComponent},
          {path: '', component: AlbumListComponent, pathMatch: 'full'},
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(catalogRoutes),
  ],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
