import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { filter, pluck, switchMap } from 'rxjs/operators';
import { ArtistService } from '../..';
import { Artist } from '../../types/artist.interface';

@Component({
  selector: 'app-artist-edit',
  templateUrl: './artist-edit.component.html',
  styleUrls: ['./artist-edit.component.css']
})
export class ArtistEditComponent implements OnInit {

  public artistData: Artist = null;

  constructor(
    private route: ActivatedRoute,
    private artistService: ArtistService) { }

  ngOnInit() {
    this.route.params.pipe(
      pluck('id'),
      filter(id => id && id > 0),
      switchMap((artistId: string) => {
        return this.artistService.getArtistById(parseInt(artistId, 10));
      })
    )
    .subscribe(
      (artistData: Artist) => {
        this.artistData = artistData;
      },
      (error) => {
        console.log('Error in getting data', error);
      }
    );
  }
}
