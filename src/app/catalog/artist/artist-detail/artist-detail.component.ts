import { Component, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { pluck, switchMap } from 'rxjs/operators';
import { ArtistService } from '../..';
import { Artist } from '../../types/artist.interface';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.css']
})
export class ArtistDetailComponent implements OnInit {

  public artistData: Artist;

  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
    private artistService: ArtistService) { }

  ngOnInit() {
    this.activeRoute.params
      .pipe(
        pluck('id'),
        switchMap((artistId) => {
          return this.artistService.getArtistById(artistId);
        })
      )
      .subscribe(
        (artistData: Artist) => {
          this.artistData = artistData;
        }
      );
  }

  public editArtist() {
    this.router.navigate(['catalog', 'artist', 'edit', this.artistData.id]);
  }

}
