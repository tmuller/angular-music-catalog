import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { ArtistService } from '../../';
import { Artist } from '../../types/artist.interface';

@Component({
  selector: 'app-artist-form',
  templateUrl: './artist-form.component.html',
  styleUrls: ['./artist-form.component.css']
})
export class ArtistFormComponent {

  @Input()
  set artistFormData(artistData: Artist) {
    if (artistData) {
      this.artistForm.patchValue(artistData);
      const musicianGroup = this.artistForm.controls.musicians as FormArray;
      // Now setting the musicians
      artistData.musicians.forEach( x => {
        musicianGroup.push(this.fb.group(x));
      });
    }
  }

  constructor(private fb: FormBuilder,
              private router: Router,
              private artistService: ArtistService) { }

  public artistForm: FormGroup = this.fb.group({
    id: [''],
    artistName: [''],
    activeSince: [''],
    retired: [''],
    musicians: this.fb.array([ ])
  });

  public readonly musicians = this.artistForm.get('musicians') as FormArray;

  public saveArtist(): void {
    const artistData = this.artistForm.value;

    let storeFct;
    if (artistData.id && artistData.id > 0) {
      storeFct = this.artistService.updateArtist.bind(this.artistService);
    } else {
      storeFct = this.artistService.storeNewArtist.bind(this.artistService);
    }
    const answer =  storeFct(artistData)
      .subscribe(
        (d) => { this.closeArtistEditForm(); }
      );
  }

  public addMusician(): void {
    this.musicians.push(this.generateNewMusician());
  }

  private generateNewMusician(): FormGroup {
    return this.fb.group({
      musicianName: ['', [Validators.required]],
      instrument: ['', [Validators.required]],
      yearJoined: [''],
      yearLeft: ['']
    });
  }

  public closeArtistEditForm(): void {
    this.router.navigate(['catalog', 'albums']);
  }
}
