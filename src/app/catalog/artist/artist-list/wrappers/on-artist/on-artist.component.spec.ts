import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnArtistComponent } from './on-artist.component';

describe('OnArtistComponent', () => {
  let component: OnArtistComponent;
  let fixture: ComponentFixture<OnArtistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnArtistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnArtistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
