import { Component, Input } from '@angular/core';
import { Artist } from '../../../../types/artist.interface';

@Component({
  selector: 'app-on-album',
  templateUrl: './on-album.component.html',
  styleUrls: ['./on-album.component.css']
})
export class OnAlbumComponent {

  @Input()
  artist: Artist;

  constructor() { }

}
