import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnAlbumComponent } from './on-album.component';

describe('OnAlbumComponent', () => {
  let component: OnAlbumComponent;
  let fixture: ComponentFixture<OnAlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnAlbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
