import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtistService } from '../..';
import { Artist } from '../../types/artist.interface';

@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.css']
})
export class ArtistListComponent implements OnInit {

  constructor(
    private artistService: ArtistService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  public listOfArtists: Artist[] = [];

  ngOnInit() {
    this.activatedRoute.data
      .subscribe(
        (listOfArtists: {artists: Artist[]}) => {
          this.listOfArtists = listOfArtists.artists;
        }
      );
  }

}
