import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { delay, take } from 'rxjs/operators';
import { Artist } from '../types/artist.interface';
import { ArtistService } from './artist.service';

@Injectable({
  providedIn: 'root'
})
export class ArtistResolverService implements Resolve<Artist[]> {

  constructor(
    private artistService: ArtistService
  ) { }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Artist[]> {
    return this.artistService.getArtistsByName('').pipe(take(1));
  }

}

