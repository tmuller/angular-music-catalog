import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Artist } from '../types/artist.interface';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  private apiUrl = 'http://localhost:3000/artists';

  constructor(private http: HttpClient) { }

  public storeNewArtist(artistData: Artist): Observable<Artist> {
   return this.http.post<Artist>(this.apiUrl, artistData);
  }

  public updateArtist(artistData: Artist): Observable<Artist> {
    return this.http.put<Artist>(`${this.apiUrl}/${artistData.id}`, artistData);
  }

  public getArtistsByName(name: string): Observable<Artist[]> {
    let urlParameters = {};
    if (name) urlParameters = {params: {'artistName_like': name}};
    return this.http.get<Artist[]>(this.apiUrl, urlParameters);
  }

  public getArtistById(id: number): Observable<Artist> {
    return this.http.get<Artist>(this.apiUrl + '/' + id.toString());
  }

}
