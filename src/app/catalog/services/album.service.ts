import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Album } from '../types/album.interface';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  private apiUrl = 'http://localhost:3000/albums';

  constructor(private http: HttpClient) { }

  public getAllAlbums(): Observable<Album[]> {
    return this.http.get<Album[]>(`${this.apiUrl}`);
  }

  public getAlbumWithId(id: number): Observable<Album> {
    return this.http.get<Album>(`${this.apiUrl}/${id}`);
  }

  public storeAlbum(albumData: Album, id?: number): Observable<Album> {
    if (id) {
      return this.http.put<Album>(`${this.apiUrl}/${id}`, albumData);
    } else {
      return this.http.post<Album>(this.apiUrl, albumData);
    }
  }

}
