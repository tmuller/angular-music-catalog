import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CatalogRoutingModule } from './catalog-routing.module';
import {
  AlbumListComponent,
  AlbumFormComponent,
  AlbumEditComponent,
  ArtistDetailComponent,
  ArtistEditComponent,
  ArtistFormComponent,
  ArtistListComponent, } from '.';
import { FormTimeEntryComponent } from './components/form-time-entry/form-time-entry.component';
import { AlbumService } from './services/album.service';
import { ArtistService } from './services/artist.service';
import { CatalogComponent } from './catalog.component';
import { CoverListComponent } from './album/album-list/cover-list/cover-list.component';
import { NumberedListComponent } from './album/album-list/numbered-list/numbered-list.component';
import { BaseListComponent } from './album/album-list/base-list/base-list.component';
import { CoverWithSongsComponent } from './album/album-list/cover-with-songs/cover-with-songs.component';

@NgModule({
  declarations: [
    AlbumListComponent,
    AlbumFormComponent,
    AlbumEditComponent,
    ArtistFormComponent,
    ArtistEditComponent,
    ArtistListComponent,
    ArtistDetailComponent,
    BaseListComponent,
    CatalogComponent,
    CoverListComponent,
    CoverWithSongsComponent,
    FormTimeEntryComponent,
    NumberedListComponent,
  ],
  entryComponents: [
    BaseListComponent,
    CoverListComponent,
    NumberedListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CatalogRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    HttpClient,
    ArtistService,
    AlbumService,
  ]
})
export class CatalogModule { }
