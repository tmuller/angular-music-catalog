export * from './album/album-edit/album-edit.component';
export * from './album/album-form/album-form.component';
export * from './album/album-list/album-list.component';
export * from './artist/artist-edit/artist-edit.component';
export * from './artist/artist-form/artist-form.component';
export * from './artist/artist-list/artist-list.component';
export * from './artist/artist-detail/artist-detail.component';
export * from './services/artist.service';
