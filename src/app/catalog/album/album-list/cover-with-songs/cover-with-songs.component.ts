import { Component } from '@angular/core';
import { AbstractList } from '../abstract-list/abstract-list.component';

@Component({
  selector: 'app-cover-with-songs',
  templateUrl: './cover-with-songs.component.html',
  styleUrls: ['./cover-with-songs.component.css']
})
export class CoverWithSongsComponent extends AbstractList {

  constructor() {
    super();
  }

}
