import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { Album } from '../../../types/album.interface';
import { AbstractList } from '../abstract-list/abstract-list.component';
import { CoverListComponent } from '../cover-list/cover-list.component';
import { CoverWithSongsComponent } from '../cover-with-songs/cover-with-songs.component';
import { NumberedListComponent } from '../numbered-list/numbered-list.component';

@Component({
  selector: 'base-list',
  templateUrl: './base-list.component.html',
  styleUrls: ['./base-list.component.css']
})
export class BaseListComponent implements OnInit, OnDestroy, OnChanges {

  @ViewChild('albumListStyle', {read: ViewContainerRef, static: true})
  albumListContainer: ViewContainerRef;

  @Input() showContentStyle: string;

  @Input() albumList: Album[] = [];

  private contentStyles = {
    list: NumberedListComponent,
    covers: CoverListComponent,
    coversWithList: CoverWithSongsComponent,
  };

  private componentReference: ComponentRef<{}>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
  ) { }

  ngOnInit() {
    this.showContentStyle = 'coversWithList';
    this.instantiateViewComponent(this.showContentStyle);
  }

  ngOnDestroy() {
    this.destroyChildComponent();
  }

  ngOnChanges() {
    this.destroyChildComponent();
    this.instantiateViewComponent(this.showContentStyle);
  }

  private instantiateViewComponent(componentName: string) {
    const componentType = this.provideListComponent(componentName);
    const factoryInstance = this.componentFactoryResolver.resolveComponentFactory(componentType);
    this.componentReference = this.albumListContainer.createComponent(factoryInstance);

    const instance = this.componentReference.instance as AbstractList;
    instance.albumListToRender = this.albumList;
  }

  private destroyChildComponent() {
    if (this.componentReference) {
      this.componentReference.destroy();
      this.componentReference = null;
    }
  }

  private provideListComponent(componentStyle: string) {
    return this.contentStyles[componentStyle] || this.contentStyles.list;
  }
}
