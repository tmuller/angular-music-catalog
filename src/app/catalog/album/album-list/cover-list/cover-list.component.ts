import { Component } from '@angular/core';
import { AbstractList } from '../abstract-list/abstract-list.component';

@Component({
  selector: 'app-cover-list',
  templateUrl: './cover-list.component.html',
  styleUrls: ['./cover-list.component.css']
})
export class CoverListComponent extends AbstractList {

  constructor() {
    super();
  }

}
