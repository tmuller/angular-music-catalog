import { Router } from '@angular/router';
import { Album } from '../../../types/album.interface';

export abstract class AbstractList {
  public albumListToRender: Album[];
}
