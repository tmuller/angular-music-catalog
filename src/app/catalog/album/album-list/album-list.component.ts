import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlbumService } from '../../services/album.service';
import { Album } from '../../types/album.interface';

@Component({
  selector: 'app-album-list-component',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.css']
})
export class AlbumListComponent implements OnInit {

  public currentStyle = 'coversWithList';

  public albumList: Album[] = [];

  constructor(
    private router: Router,
    private albumService: AlbumService,
  ) { }

  ngOnInit() {
    this.albumService.getAllAlbums()
      .subscribe(
        (albumList: Album[]) => {
          this.albumList = albumList;
        }
      );
  }

  public openNewAlbumForm() {
    this.router.navigate(['catalog', 'albums', 'detail']);
  }

  public onChangeAlbumDisplayClick($event) {
    this.currentStyle = $event;
  }
}
