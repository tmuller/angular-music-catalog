import { Component } from '@angular/core';
import { AbstractList } from '../abstract-list/abstract-list.component';

@Component({
  selector: 'app-numbered-list',
  templateUrl: './numbered-list.component.html',
  styleUrls: ['./numbered-list.component.css']
})
export class NumberedListComponent extends AbstractList {

  constructor() {
    super();
  }
}
