import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumFormComponentComponent } from './album-form-component.component';

describe('AlbumFormComponentComponent', () => {
  let component: AlbumFormComponentComponent;
  let fixture: ComponentFixture<AlbumFormComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumFormComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumFormComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
