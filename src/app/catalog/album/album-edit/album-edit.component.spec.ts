import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumEditComponentComponent } from './album-edit-component.component';

describe('AlbumEditComponentComponent', () => {
  let component: AlbumEditComponentComponent;
  let fixture: ComponentFixture<AlbumEditComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumEditComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumEditComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
