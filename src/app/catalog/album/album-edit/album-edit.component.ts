import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fromEvent, Observable, Subject } from 'rxjs';
import { debounceTime, filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { ArtistService } from '../..';
import { AlbumService } from '../../services/album.service';
import { Album } from '../../types/album.interface';
import { Artist } from '../../types/artist.interface';
import { Song } from '../../types/song.interface';

@Component({
  selector: 'app-album-edit-component',
  templateUrl: './album-edit.component.html',
  styleUrls: ['./album-edit.component.css']
})
export class AlbumEditComponent implements OnInit, OnDestroy {

  public albumForm: FormGroup = this.fb.group({
    id: [''],
    artist: ['', [Validators.required]],
    albumName: ['', [Validators.maxLength(35)]],
    albumYear: ['', [Validators.min(1935), Validators.max(new Date().getFullYear())]],
    genre: [''],
    rating: ['', [Validators.max(5)]],
    discs: this.fb.array([])
  });

  public readonly discs = this.albumForm.get('discs') as FormArray;

  private artistName$: Observable<Artist[]>;

  private subscriptionTermination = new Subject<boolean>();

  public artistAutoCompleteContent: Artist[] = [];

  constructor(
    public router: Router,
    public routeData: ActivatedRoute,
    private artistService: ArtistService,
    private albumService: AlbumService,
    public fb: FormBuilder) {}

  public ngOnInit() {
    this.artistName$ = fromEvent(document.getElementById('artist'), 'keyup')
      .pipe(
        takeUntil(this.subscriptionTermination),
        debounceTime(200),
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter((fieldContent: string) => fieldContent.length > 2),
        switchMap(fieldContent => this.artistService.getArtistsByName(fieldContent))
      );
    this.artistName$.subscribe(
      (artistEntities: Artist[]) => this.artistAutoCompleteContent = artistEntities
    );

    const albumId: number = parseInt(this.routeData.snapshot.paramMap.get('albumId'), 10);
    this.albumService.getAlbumWithId(albumId)
      .subscribe(
        (album: Album) => {
          this.albumForm.patchValue(album);
          for (const diskNum in album.discs) {
            this.addDisk(false, album.discs[diskNum].discNumber);
            for (const song of album.discs[diskNum].songs) {
              this.addSong(parseInt(diskNum, 10), song);
            }
          }
        }
      );

  }

  public ngOnDestroy() {
    this.subscriptionTermination.next(true);
    this.subscriptionTermination.complete();
  }

  public enterSelectedArtistIntoFormField(artistInfo: Artist): void {
    this.albumForm.patchValue({artist: artistInfo.artistName});
    this.artistAutoCompleteContent = [];
  }

  public addDisk(presetWithEmpty: boolean = true, discNumber?: number) {
    const orderNum = discNumber || this.discs.length;
    this.discs.push(this.fb.group({
      discNumber: [orderNum],
      songs: this.fb.array([])
    }));
    if (presetWithEmpty) this.addSong(orderNum);
  }

  public getSongs(diskNum: number): FormArray {
    const diskControlInstance = this.discs.controls[diskNum || 0] as FormGroup;
    return diskControlInstance.get('songs') as FormArray;
  }

  public addSong(discNumber: number, songData?: Song) {
    const songGroup = this.createEmptySongGroup();
    if (songData) songGroup.patchValue(songData);
    this.getSongs(discNumber).push(songGroup);
  }

  public createEmptySongGroup() {
    return this.fb.group({
      trackNumber: ['', [Validators.min(1)]],
      artist: [''],
      name: ['', [Validators.required]],
      length: ['', [Validators.min(180), Validators.max(5000)]], // Length in minutes,
      rating: ['', [Validators.max(5)]],
    });
  }


  public submitAlbumData(): void {
    const albumData = this.albumForm.value;
    const albumId = albumData.id;
    this.albumService.storeAlbum(albumData, albumId)
      .subscribe(
        reply => {
          this.router.navigate(['catalog', 'albums']);
        }
      );
  }

  public deleteSong(disc: number, arrayIndex: number) {
    this.getSongs(disc).removeAt(arrayIndex);
  }
}
