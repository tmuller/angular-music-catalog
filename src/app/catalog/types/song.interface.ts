import { Artist } from './artist.interface';

export interface Song {
  trackNumber: number;  // Dealing with incomplete disks?
  artist?: Artist;
  name: string;
  length: number; // in seconds
  genre?: string;
  rating?: number;
}
