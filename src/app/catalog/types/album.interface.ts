import { Artist } from './artist.interface';
import { RecordingMedium } from './recording-medium.interface';

/**
 * What if it's a compilation and there are multiple artists on a disc?
 */

export interface Album {
  id?: number;
  artist: Artist;
  albumName: string;
  albumYear: number;
  genre: string;
  rating: number;
  discs: RecordingMedium[];
}

