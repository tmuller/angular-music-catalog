import { Song } from './song.interface';

export interface RecordingMedium {
  discNumber: number;
  songs: Song[];
}
