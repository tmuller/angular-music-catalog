export interface Musician {
  musicianName: string;
  instrument: string;
  yearJoined: number;
  yearLeft: number;
}

