import { Musician } from './musician.interface';

export interface Artist {
  id?: number;
  artistName: string;
  activeSince: number;
  retired: boolean;
  musicians: Musician[];
}

