import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CatalogModule } from './catalog/catalog.module';
import { AppRoutingModule } from './app-routing.module';
import { FormTimeEntryComponent } from './catalog/components/form-time-entry/form-time-entry.component';
import { TlistComponent } from './test-list/tlist/tlist.component';
import { TestWrapperComponent } from './test-wrapper/test-wrapper.component';

@NgModule({
  declarations: [
    AppComponent,
    TestWrapperComponent,
    TlistComponent,
    TestWrapperComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CatalogModule,
    RouterModule,
    AppRoutingModule,
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
