import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeadingZeroesPipe } from './leading-zeroes.pipe';

@NgModule({
  declarations: [
    LeadingZeroesPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LeadingZeroesPipe
  ],
})
export class SharedModule { }
