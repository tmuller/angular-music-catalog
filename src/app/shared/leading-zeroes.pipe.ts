import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'leadingZeroes'
})
export class LeadingZeroesPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    const truncatedTime = value.match(/[1-9]?\d:\d{2}$/);
    return truncatedTime[0];
  }

}
