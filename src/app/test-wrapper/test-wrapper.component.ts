import { Component, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-test-wrapper',
  templateUrl: './test-wrapper.component.html',
  styleUrls: ['./test-wrapper.component.css']
})
export class TestWrapperComponent {

  @ViewChild('customWrapperTemplate', {static: true})
  wrapperTemplate: TemplateRef<any>;

  constructor() { }

}
