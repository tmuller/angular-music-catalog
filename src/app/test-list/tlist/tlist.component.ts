import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-tlist',
  templateUrl: './tlist.component.html',
  styleUrls: ['./tlist.component.css']
})
export class TlistComponent {

  @Input()
  listTemplate: TemplateRef<any>;

  public data = {
    people: [
      {test: 1, name: 'Ludwig'},
      {test: 2, name: 'Anastasia'},
      {test: 3, name: 'Lynn'},
      {test: 4, name: 'Jim'},
      {test: 5, name: 'Bruce'},
      {test: 6, name: 'Antje'},
    ]
  };

  constructor() { }

}
