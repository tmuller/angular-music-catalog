import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title = 'catalog';

  constructor(public router: Router) {}

  public navigate() {
    this.router.navigateByUrl('/catalog/new-album(sidebar:catalog/new-album)');
  }

  ngOnInit() {
    // setTimeout(this.navigate.bind(this), 5000);
  }

}
